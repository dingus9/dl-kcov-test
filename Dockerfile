FROM centos:centos7 as builder

# Install build tools
RUN yum groupinstall -y "Development Tools" \
  && yum -y install epel-release \
  && yum -y --enablerepo base-debuginfo install \
      cmake \
      ninja-build \
      python3-pip \
      libcurl-devel \
      zlib-devel \
      libdwarf-devel \
      binutils-devel \
      binutils-debuginfo \
      elfutils-devel \
      elfutils-libelf-devel;

ADD https://github.com/SimonKagstrom/kcov/archive/v37.tar.gz /src/kcov-v37.tar.gz

# Build kcov
RUN cd /src \
  && tar -xzf kcov-v37.tar.gz \
  && mkdir /src/kcov-37/build \
  && cd /src/kcov-37/build/ \
  && cmake -G 'Ninja' .. \
  && cmake --build . \
  && cmake --build . --target install

FROM centos:centos7

RUN yum install -y \
        binutils \
        libcurl \
        libdwarf \
        zlib \
    && yum clean --enablerepo base-debuginfo all --verbose;

COPY --from=builder /usr/local/bin/kcov* /usr/local/bin/
COPY --from=builder /usr/local/share/doc/kcov /usr/local/share/doc/kcov

CMD ["/usr/local/bin/kcov"]
